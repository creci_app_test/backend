const { Router } = require('express');
const router = Router();
const Project = require('../models/Project');
const { verifyToken, verifyLeaderToken } = require('../tokenAuth');

router.get('/', verifyToken, (req, res) => {
    Project.find()
		.then((users) => {
			return res.status(200).json(users);
		})
		.catch((error) => {
			return res.status(400).json(error);
		});
});

router.post('/', verifyLeaderToken, async (req, res) => {
    const project = req.body;
	try {
		const newProject = new Project(project);
		await newProject.save();
		return res.status(200).json(newProject);
	} catch (error) {
		return res.status(400).json(error);
	}
});

router.get('/:_id', verifyToken, async (req, res) => {
    const { _id } = req.params;
	try {
		const project = await Project.findById(_id);
		return res.status(200).json(project);
	} catch (error) {
		return res.status(400).json(error);
	}
});

router.patch('/:_id', verifyToken, async (req, res) => {
    const { _id } = req.params;
	const project = req.body;
	try {
		await Project.findByIdAndUpdate(_id, project);
		return res.status(200).json(project);
	} catch (error) {
		return res.status(400).json(error);
	}
});

router.delete('/:_id', verifyLeaderToken, async (req, res) => {
    const { _id } = req.params;
	try {
		const deletedProject = await Project.findByIdAndDelete(_id);
		return res.status(200).json(deletedProject);
	} catch (error) {
		return res.status(400).json(error);
	}
});

module.exports = router;
