const { Router } = require('express');
const router = Router();
const User = require('../models/User');
const jwt = require('jsonwebtoken');
const { verifyToken, verifyLeaderToken } = require('../tokenAuth');

router.get('/', verifyLeaderToken, (req, res) => {
	User.find()
		.then((users) => {
			return res.status(200).json(users);
		})
		.catch((error) => {
			return res.status(400).json(error);
		});
});

router.post('/', verifyLeaderToken, async (req, res) => {
	const user = req.body;
	try {
		const newUser = new User(user);
		await newUser.save();
		return res.status(200).json(newUser);
	} catch (error) {
		return res.status(400).json(error);
	}
});

router.get('/:_id', verifyLeaderToken, async (req, res) => {
	const { _id } = req.params;
	try {
		const user = await User.findById(_id);
		return res.status(200).json(user);
	} catch (error) {
		return res.status(400).json(error);
	}
});

router.patch('/:_id', verifyToken, async (req, res) => {
	const { _id } = req.params;
	const user = req.body;
	try {
		await User.findByIdAndUpdate(_id, user);
		return res.status(200).json(user);
	} catch (error) {
		return res.status(400).json(error);
	}
});

router.delete('/:_id', verifyLeaderToken, async (req, res) => {
	const { _id } = req.params;
	try {
		const deletedUser = await User.findByIdAndDelete(_id);
		return res.status(200).json(deletedUser);
	} catch (error) {
		return res.status(400).json(error);
	}
});

router.post('/login', async (req, res) => {
	const loginForm = req.body;
	try {
		const logedUser = await User.findOne(loginForm);
		const token = jwt.sign({ _id: logedUser._id }, 'creci');
		return res.status(200).json({token, logedUser});
	} catch (error) {
		if (error.length > 0) {
			return res.status(400).json(error);
		} else {
			return res.status(401).json('Credenciales incorrectas');
		}
	}
});

module.exports = router;
