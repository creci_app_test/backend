const { Router } = require('express');
const router = Router();
const Task = require('../models/Task');
const { verifyToken, verifyLeaderToken } = require('../tokenAuth');

router.get('/', verifyToken, (req, res) => {
    Task.find()
		.then((users) => {
			return res.status(200).json(users);
		})
		.catch((error) => {
			return res.status(400).json(error);
		});
});

router.post('/', verifyLeaderToken, async (req, res) => {
    const task = req.body;
	try {
		const newTask = new Task(task);
		const savedTask = await newTask.save();
		return res.status(200).json(savedTask);
	} catch (error) {
		return res.status(400).json(error);
	}
});

router.get('/:_id', verifyToken, async (req, res) => {
    const { _id } = req.params;
	try {
		const task = await Task.findById(_id);
		return res.status(200).json(task);
	} catch (error) {
		return res.status(400).json(error);
	}
});

router.patch('/:_id', verifyToken, async (req, res) => {
    const { _id } = req.params;
	const task = req.body;
	try {
		const updatedTask = await Task.findByIdAndUpdate(_id, task);
		return res.status(200).json(updatedTask);
	} catch (error) {
		return res.status(400).json(error);
	}
});

router.delete('/:_id', verifyLeaderToken, async (req, res) => {
    const { _id } = req.params;
	try {
		const deletedTask = await Task.findByIdAndDelete(_id);
		return res.status(200).json(deletedTask);
	} catch (error) {
		return res.status(400).json(error);
	}
});

module.exports = router;
