const { Schema, model } = require('mongoose');

const userSchema = new Schema({
    firstName: {
        type: String,
        trim: true,
    },
    lastName: {
        type: String,
        trim: true,
    },
    email: {
        type: String,
        trim: true,
        required: true,
    },
    password: {
        type: String,
        trim: true,
        required: true,
        minlength: 12,
    },
    rol: {
        type: String,
        enum: ['Leader', 'Developer'],
    },
}, { collection: 'users' });

module.exports = model('User', userSchema);