const { Schema, model } = require('mongoose');

const taskSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: true,
    },
    description: {
        type: String,
        trim: true,
        required: true,
    },
    timeReports: [
        {
            activityDescription: {
                type: String,
            },
            timeExpended: {
                type: Number,
            },
            timeExpendedUpdateDate: {
                type: Date,
            },
        },
    ],
    delegate: {
        type: Schema.Types.ObjectId,
        ref: 'User',
    },
}, { collection: 'tasks' });

module.exports = model('Task', taskSchema);