const { Schema, model } = require('mongoose');

const projectSchema = new Schema({
    name: {
        type: String,
        trim: true,
        required: true,
    },
    description: {
        type: String,
        trim: true,
        required: true,
    },
    usersTimeReports: [
        {
            activityDescription: {
                type: String,
            },
            timeExpended: {
                type: Number,
            },
            timeExpendedUpdateDate: {
                type: Date,
            },
            user: {
                type: Schema.Types.ObjectId,
                ref: 'User',
            }
        },
    ],
    delegates: [
        {
            type: Schema.Types.ObjectId,
            ref: 'User',
        },
    ],
}, { collection: 'projects' });

module.exports = model('Projects', projectSchema);