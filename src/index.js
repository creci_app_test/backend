const express = require('express');
const app = express();
require('dotenv').config();
const cors = require('cors');
const morgan = require('morgan');
require('colors');
require('./databases/mongodb_database');

app.use(cors());
app.use(morgan(function (tokens, req, res) {
    return [
        tokens.method(req, res),
        tokens.url(req, res),
        tokens.status(req, res),
        tokens.res(req, res, 'content-length'), '-',
        tokens['response-time'](req, res), 'ms'
    ].join(' ')
}));
app.use(express.json());

app.use('/api/v1.0/user', require('./routes/User'));
app.use('/api/v1.0/task', require('./routes/Task'));
app.use('/api/v1.0/project', require('./routes/Project'));

app.listen(process.env.APP_PORT, process.env.APP_HOST, (error) => {
    if (error) return console.log(error);
    console.log('Servidor: ' + 'FUNCIONANDO' .green, '\nPuerto:', process.env.APP_PORT);
});