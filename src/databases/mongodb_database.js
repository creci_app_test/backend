const mongoose = require('mongoose');
require('colors');

const uri = 'mongodb://localhost/' + process.env.MONGO_DB;
const options = {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    user: process.env.MONGO_USER,
    pass: process.env.MONGO_PASS,
};

mongoose
	.connect(uri, options)
	.then(() => console.log('Base de datos: ' + 'FUNCIONANDO' .green))
	.catch((error) => {
        console.log('Base de datos: ' + 'ERROR' .red)
        console.error(error)
    });
