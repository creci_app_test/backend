const jwt = require('jsonwebtoken');
const User = require('./models/User');

async function verifyToken(req, res, next) {
	if (!req.headers.authorization) {
		return res.status(401).send('Token inexistente');
	}
	const token = req.headers.authorization;
	if (token === null) {
		return res.status(401).send('Token vacio');
	}

	const payload = jwt.verify(token, process.env.TOKEN_SECRET);
	const user = await User.findById(payload._id);
    if (user) {
        req.userId = payload._id;
        next();
    } else {
        res.status(401).send('Acción no autorizada');
    }
}

async function verifyLeaderToken(req, res, next) {
	if (!req.headers.authorization) {
		return res.status(401).send('Token inexistente');
	}
	const token = req.headers.authorization;
	if (token === null) {
		return res.status(401).send('Token vacio');
	}
    const payload = jwt.verify(token, process.env.TOKEN_SECRET);
    const user = await User.findById(payload._id);
    if (user.rol === 'Leader') {
        req.userId = payload._id;
        next();
    } else {
        res.status(401).send('Acción no autorizada');
    }
}

module.exports = {verifyLeaderToken, verifyToken};